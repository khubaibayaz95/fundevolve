import React, { useState, Fragment } from "react";

import { Stepper } from 'react-form-stepper';

import Step1 from "./MultiStep1";
import Step2 from "./MultiStep2";
import Step3 from "./MultiStep3";
import Step4 from "./MultiStep4";
import Step5 from "./MultiStep5";
import Step6 from "./MultiStep6";
import Submit from "./MultiStepSubmit";

import { Container, Row, Col, Button, Modal, Card } from 'react-bootstrap';

export const MultiStepForm = () => {
  const [currentStep, setCurrentStep] = useState(1);
  const [formData, setFormData] = useState({
    name: "",
    email: "",
    street: "",
    city: "",
    postcode: "",
    comments: "",
  });

  const [show, setShow] = useState(false);
  const [modalTitle, setModalTitle] = useState("CREATE A GOAL");

  const handleChange = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value,
    });
  };
  const next = () => {
    if(currentStep + 1 == 7) {
      setShow(false)
      setCurrentStep(1);
    }
    else {
      setCurrentStep(currentStep + 1);
    }
  };
  const back = () => {
    setCurrentStep(currentStep - 1);
  };

  const saveForm = () => {
    setShow(false)
    setCurrentStep(1);
    
    // alert(formData.name + formData.email + formData.street + formData.city + formData.postcode + formData.comments)
    console.log('')
  }

  return (
    <Fragment>
      <Container>
        <Card className="goals-card text-center">
          <Card.Body>
            <Row className="justify-content-md-center">
              <Row>
                <Col className="h-100 text-center mt-5 pt-5">
                  <Button variant="light" onClick={() => setShow(true)}>
                   Create Goal
                  </Button>
                </Col>
              </Row>
            </Row>
          </Card.Body>
        </Card>
      </Container>

      <Modal
        show={show}
        onHide={() => setShow(false)}
        // dialogClassName="modal-90w modal-height"
        aria-labelledby="contained-modal-title-vcenter"
        backdrop="static"
        size="lg"
        centered
      >
        <Modal.Header className="modal-header-bg" closeButton>
          <div style={{display: 'flex', flexWrap: 'wrap', width: '100%'}}>
            <div style={{width: '100%', textAlign: 'center'}}><p className="f-w-b">{currentStep >= 2 ? "CRUSH CREDIT CARD DEBT" : "CREATE A GOAL"}</p></div>
            <div style={{width: '100%'}}>
              <Stepper
                steps={[{ label: currentStep == 1 ? 'Define' : '' }, { label: currentStep == 2 ? 'Describe' : '' }, { label: currentStep == 3 ? 'Specify' : ''}, { label: currentStep == 4 ? 'Collaborate' : ''}, { label: currentStep == 5 ? 'Visualize' : '' }, { label: currentStep == 6 ? 'Get Started' : ''}]}
                activeStep={currentStep-1}
              />
            </div>
          </div>
        </Modal.Header>

        <Modal.Body className="modal-body-height budgett">
          <Container>
            {currentStep == 1 && (<Step1
              data={formData}
              handleChange={handleChange}
              next={next}
            />)}

            {currentStep == 2 && (<Step2
              data={formData}
              handleChange={handleChange}
              next={next}
              back={back}
            />)}

            {currentStep == 3 && (<Step3
              data={formData}
              handleChange={handleChange}
              next={next}
              back={back}
            /> )}

          {currentStep == 4 && (<Step4
              data={formData}
              handleChange={handleChange}
              next={next}
              back={back}
            /> )}

            {currentStep == 5 && (<Step5
              data={formData}
              handleChange={handleChange}
              next={next}
              back={back}
            /> )}

            {currentStep == 6 && (<Step6
              data={formData}
              handleChange={handleChange}
              next={next}
              back={back}
              saveForm={saveForm}
            /> )}
          </Container>
        </Modal.Body>
      </Modal>
    </Fragment>
  )
}