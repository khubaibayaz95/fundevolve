import React from "react";
import { Modal, Button, Container } from 'react-bootstrap';
import Input from '@material-ui/core/Input';
import OutlinedInput from '@material-ui/core/OutlinedInput';

import FilledInput from '@material-ui/core/FilledInput';

import FormHelperText from '@material-ui/core/FormHelperText';

import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';
import InputBase from '@material-ui/core/InputBase';

const Step2 = (props) => {
  const { data, handleChange, next, back } = props;
  return (
    <Container >
      <form>
        
        <label className="uppercase fs-10 f-w-b" htmlFor="name">WHICH CATEGORY DO YOU WANT TO MANAGE YOUR SPENDINGS</label>
          <br/>

          <select className='form-control' style={{width:'50%', backgroundColor:'lightgrey', color:'black'}}>

              <option>Select Category</option>
              <option>Restaurants</option>
          </select>
       
      <br/>
      <label className="uppercase fs-10 f-w-b" htmlFor="name">DO YOU WANT TO INCREASE OR DECREASE YOUR BUDGET?</label>
          <br/>
      <FormControl>


      </FormControl>

     
          <Checkbox
        defaultChecked
        color="default"
       
        inputProps={{
          'aria-label': 'checkbox with default color',
        }}
      /> INCREASE
      <br/>
      <Checkbox
        defaultChecked
        color="default"
        
        inputProps={{
          'aria-label': 'checkbox with default color',
        }}
      /> DECREASE

      <br/>
      <label className="uppercase fs-10 f-w-b" htmlFor="name">BY WHAT PERCENTAGE?</label>
          <br/>
          <input type='text' placeholder='%' className='form-control' style={{width:'25%', backgroundColor:'lightgrey', color:'black'}}/>

          <br/>
          <label className="uppercase fs-10 f-w-b" htmlFor="name">SET A DEADLINE FOR YOUR GOAL</label>
          <br/> 
          <div className='row sm-1 m-2' style={{alignContent:'left'}}>
          <select className='form-control' style={{width:'20%', backgroundColor:'lightgrey', color:'black'}}>

            <option>Month</option>
            <option>JANUARY</option>
            </select>
            <select className='form-control' style={{width:'20%', backgroundColor:'lightgrey', color:'black'}}>

            <option>Day</option>
            <option>JANUARY</option>
            </select>
            <select className='form-control' style={{width:'15%', backgroundColor:'lightgrey', color:'black'}}>

            <option>Year</option>
            <option>JANUARY</option>
            </select>



          </div>
         





        <div className="modal-footer-buttons">
          <Button className="min-width-100" size="sm" variant="dark" style={{ float: 'left' }} onClick={back}>&#60;&#60; Back</Button>{" "}
          <Button className="min-width-100" size="sm" variant="dark" style={{ float: 'right' }} onClick={next}>Next &#62;&#62;</Button>
        </div>



      </form>
    </Container>
  );
};
export default Step2;