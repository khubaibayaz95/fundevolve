import React from "react";
import { Modal, Button, Container } from 'react-bootstrap';
const Step5 = (props) => {
  const { data, handleChange, next, back } = props;
  const hiddenFileInput = React.useRef(null);



  const handleClick = event => {
    hiddenFileInput.current.click();
  };

  const handleChangefile = event => {
    const fileUploaded = event.target.files[0];
    // props.handleFile(fileUploaded);
  };
  return (
    <Container>
      <form>
        <p>
          <label className="uppercase fs-10 f-w-b" htmlFor="postcode">UPLOAD PHOTOS FOR INSPIRATION</label>
          <br/>

          <Button size="sm" variant='dark' style={{borderRadius:'20px'}} onClick={handleClick}>
        Image Upload
      </Button>
      <input
        type="file"
        ref={hiddenFileInput}
        onChange={handleChangefile}
        style={{display: 'none'}}
      />
          {/* <input
            type="file"
            variant='dark'
            name="postcode"
            value={data.postcode}
            onChange={handleChange}
            style={{

              backgroundColor:'black',
              color:'white',
              borderRadius:'50px'



            }}
          /> */}


        </p>
        <br/>
        <br/>

        <label className="uppercase fs-10 f-w-b">ADD A MOTIVATIONAL QUOTE TO ACHIEVE YOUR GOAL</label>
        <textarea name="goalDesc" rows='5' className='form-control custom-input f-w-b' placeholder='Type quote or motivational words'>

        </textarea>


        <div className="modal-footer-buttons">
          <Button className="min-width-100" size="sm" variant="dark" style={{ float: 'left' }} onClick={back}>&#60;&#60; Back</Button> {" "}
          <Button className="min-width-100" size="sm" variant="dark" style={{ float: 'right' }} onClick={next}>Next &#62;&#62;</Button>
        </div>
      </form>
    </Container>
  );
};
export default Step5;