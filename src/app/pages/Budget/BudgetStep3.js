import React from "react";
import { Col, Label } from "react-bootstrap";
import { Modal, Button, Container ,Row, FormGroup} from 'react-bootstrap';

const Step4 = (props) => {
  const { data, handleChange, next, back } = props;
  return (
    <Container>
      <form>
        <p>
          <label 
            className="uppercase fs-10 f-w-b"
          
            htmlFor="text"><b>DO YOU WANT TO COLLABORATE ON THIS GOAL WITH ANONE ELSE?</b></label>
          <br/>

<div> 
 {/* <Row sm={6}>
 <input  type='checkbox' value='YES'/><span>YES</span>
    <input  type='checkbox' value='NO'/><span>NO </span>
 </Row>
     */}


<FormGroup check>
  <Row sm={6}>
 
       <Col>
       <label check>
          <input className="custom-checkbox" type="checkbox" name='checkbox' /> YES
        </label>
       </Col>
         <Col>
       <label check>
          <input className="custom-checkbox" type="checkbox" name='checkbox' /> NO
        </label>
       </Col>

       
  </Row>
        
      </FormGroup>




</div>
        
          
        </p>

        <div className="modal-footer-buttons">
          <Button className="min-width-100" size="sm" variant="dark" style={{ float: 'left' }} onClick={back}>&#60;&#60; Back</Button> {" "}
          <Button className="min-width-100" size="sm" variant="dark" style={{ float: 'right' }} onClick={next}>Next &#62;&#62;</Button>
        </div>
      </form>
    </Container>
  );
};
export default Step4;