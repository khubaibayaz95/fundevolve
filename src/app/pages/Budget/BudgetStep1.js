import { Input } from "@material-ui/core";
import React from "react";
import { Modal, Button, Container } from 'react-bootstrap';



const Step1 = (props) => {
  const { data, handleChange, next } = props;
  return (
    <Container>
      <form>
        <p>
          <label htmlFor="name" style={{fontSize:18}}><b>Let's build a budget...</b></label> <br/>
      
        </p>
        <label className="uppercase fs-10 f-w-b" htmlFor="name">NAME YOUR GOAL</label>
          <br/>
          <input
            type="text"
            name="projectName"
            value={data.projectName}
            className='form-control custom-input f-w-b'
            placeholder="Enter Project Name"
            onChange={handleChange}
          />
          <br/>
          <label className="uppercase fs-10 f-w-b" htmlFor="name">DESCRIBE YOUR GOAL</label>
          <br/>
          <input
            type="text"
            name="projectName"
            value=""
            className='form-control custom-input f-w-b'
            placeholder="Enter Project Name"
            onChange={handleChange}
          />
        <div className="modal-footer-buttons">
          <Button className="min-width-100" size="sm" variant="dark" style={{float: 'right'}} onClick={next}>Next &#62;&#62;</Button>
        </div>
      </form>
    </Container>
  );
};
export default Step1;