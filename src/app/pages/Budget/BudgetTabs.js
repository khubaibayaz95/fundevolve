
import React, { useState, Fragment } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import SimpleCard from '../Budget/TheBasics';
import BlissfulTab from '../Budget/TheBlissful';
import IntentionalTab from '../Budget/TheIntentional';
import VoidTab from '../Budget/TheVoid';

import { Stepper } from 'react-form-stepper';

import Step1 from "./BudgetStep1";
import Step2 from "./BudgetStep2";
import Step3 from "./BudgetStep3";
import Step4 from "./BudgetStep4";
import Step5 from "./BudgetStep5";

import { Container, Row, Col, Button, Modal, Card, Image, DropdownButton, Dropdown } from 'react-bootstrap';




function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function SimpleTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [currentStep, setCurrentStep] = useState(1);
  const [formData, setFormData] = useState({
    name: "",
    projectName: "",
    collabGoalA: false,
    collabGoalB: false,
    email: "",
    street: "",
    city: "",
    postcode: "",
    comments: "",
  });
  const ITEM_HEIGHT = 48;
  const options = [
    'Edit',
    'Share',
    'Save',
    'Delete'
  ];


  const [show, setShow] = useState(false);
  const [modalTitle, setModalTitle] = useState("BUILD A BUDGET");

  const [goalsCount, setGoalsCount] = useState(1)

  const [ddValue, setDDValue] = useState('Show All')

  const handleChange = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value,
    });
  };
  const next = () => {
    if (currentStep + 1 == 6) {
      setShow(false)
      setCurrentStep(1);
    }
    else {
      setCurrentStep(currentStep + 1);
    }
  };
  const back = () => {
    setCurrentStep(currentStep - 1);
  };

  const updateDDVal = (e) => {
    setDDValue(e.target.value)
  }


  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  function handleClick(event) {
    setAnchorEl(event.currentTarget);
  }

  function handleClose() {
    setAnchorEl(null);
  }

  function handleClickOptions(option) {
    debugger;
  }

  function handleChangeTab(event, newValue) {
    setValue(newValue);
  }

  const saveForm = () => {
    setShow(false)
    setCurrentStep(1);

    window.location.reload();
  }

  return (
    <div className={classes.root}>
     
        <Tabs value={value} onChange={handleChangeTab} style={{

            backgroundColor:'white'
        }}>
        <Tab label="The Basics" />
        <Tab label="The Blissful"  />
        <Tab label="The Intentional" />
        <Tab label="The Void" />


        <Container>

          <Row sm={1} style={{float: 'right'}}>
          <Button 
          
          className="goals-create min-width-100" 
          size="sm" 
          variant="dark" 
          onClick={() => setShow(true)}
          style={{
           
            maxWidth:'150px',
            marginTop:'20px'
          }}
          
          >
            
            <div class="circle">+</div> Create Budget
          </Button>
    

          </Row>
        </Container>
       
      
        </Tabs>
    
      {value === 0 && <TabContainer> <SimpleCard/></TabContainer>}
      {value === 1 && <TabContainer><BlissfulTab/></TabContainer>}
      {value === 2 && <TabContainer><IntentionalTab/></TabContainer>}
      {value === 3 && <TabContainer><VoidTab/></TabContainer>}


      <Modal
        show={show}
        onHide={() => setShow(false)}
        // dialogClassName="modal-90w modal-height"
        aria-labelledby="contained-modal-title-vcenter"
        backdrop="static"
        size="lg"
        centered
      >
        <Modal.Header className="modal-header-bg" closeButton>
          <div style={{ display: 'flex', flexWrap: 'wrap', width: '100%' }}>
            <div style={{ width: '100%', textAlign: 'center' }}><p className="f-w-b">{currentStep >= 2 ? "CRUSH CREDIT CARD DEBT" : "BUILD A BUDGET"}</p></div>
            <div style={{ width: '100%' }}>
              <Stepper
                steps={[{ label: currentStep == 1 ? 'Describe' : '' }, { label: currentStep == 2 ? 'Specify' : '' }, { label: currentStep == 3 ? 'Collaborate' : '' }, { label: currentStep == 4 ? 'Visualize' : '' }, { label: currentStep == 5 ? 'Goal Created' : '' }]}
                activeStep={currentStep - 1}
              />
            </div>
          </div>
        </Modal.Header>

        <Modal.Body className="modal-body-height-budget">
          <Container>
            {currentStep == 1 && (<Step1
              data={formData}
              handleChange={handleChange}
              next={next}
            />)}

            {currentStep == 2 && (<Step2
              data={formData}
              handleChange={handleChange}
              next={next}
              back={back}
            />)}

            {currentStep == 3 && (<Step3
              data={formData}
              handleChange={handleChange}
              next={next}
              back={back}
            />)}

            {currentStep == 4 && (<Step4
              data={formData}
              handleChange={handleChange}
              next={next}
              back={back}
            />)}

            {currentStep == 5 && (<Step5
              data={formData}
              handleChange={handleChange}
              next={next}
              back={back}
              saveForm={saveForm}
            />)}

          </Container>
        </Modal.Body>
      </Modal>
    </div>
  );
}
