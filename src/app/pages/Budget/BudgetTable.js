
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import ImportExportIcon from '@material-ui/icons/ImportExport';
import Avatar from './Avatar.png'


import { useTable, usePagination, useSortBy, useFilters } from 'react-table';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  paper: {
    marginTop: theme.spacing(3),
    width: '100%',
    overflowX: 'auto',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 650,
  },
}));

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}
  
      const rows = [
        createData('Shoprite', 'Groceries', '07/01/20', '14%', '$201.87'),
        createData('Bank of America', 'Mortgage', '07/01/20', '78%', '$1965.73'),
        createData('Horizon Blue Cross', 'Insurance', '07/01/20', '64%', '$128.65'),
        createData('Comast Cable', 'Cable Streaming', '07/01/20', '30%', '143.00'),
        createData('Comast Cable', 'Cable Streaming', '07/01/20', '30%', '143.00'),
        createData('Comast Cable', 'Cable Streaming', '07/01/20', '30%', '143.00'),
        createData('Comast Cable', 'Cable Streaming', '07/01/20', '30%', '143.00'),
      //  {
        //   accountName: 'Comast Cable',
        //   type : 'Cable Streaming',
        //   transactionDate: '07/01/20',
        //   bugdget: '30%',
        //   amount: '143.00'
        // }
        
      ];


export default function BudgtTable() {
  const classes = useStyles();

  const dropdownValues = [
    'Cable/Satellite Services',
    'Child/Dependent Expenses',
    'Clothing/Shoes',
    'Groceries',
    'HealthCare/Medical',
    'Insurance',
    'Mortgages',
    'Personal Care',
    'Pets/Pet Care',
    'Rent',
    'Telephone Servies',
    'Utilities'
  ];

  const data = React.useMemo(
    () => [
      {
          accountName: 'Comast Cable',
          type : 'Cable Streaming',
          transactionDate: '07/01/20',
          budget: '30%',
          amount: '143.00'
      },
      {
        accountName: 'Comast Cable',
        type : 'Cable Streaming',
        transactionDate: '07/01/20',
        budget: '30%',
        amount: '143.00'
      }
    ],
    []
  );
  const columns = React.useMemo(
    () => [
      {
        Header: 'Account Name',
        accessor: 'accountName', // accessor is the "key" in the data
        Cell: ({ cell: { value } }) => (
                
          <div style={{fontSize:10, fontWeight:'bold'}}>
          <img style={{height:25, width:25, borderRadius:5, marginRight: 5}} src={Avatar}></img>
            {value}
          </div>
      ),
      },
      {
        Header: 'Type',
        accessor: 'type',
        Cell: ({ cell: { value } }) => (
                
          
          <select className='form-control' style={{borderRadius:0, height:35, fontWeight: 'bold'}}>
            {
              dropdownValues.map((val) => (
                <option style={{backgroundColor: 'lightgray'}}>
                  {val}
                </option>
              ))
            }
         </select>
   
      ),
      },
      {
        Header: 'Transaction Date',
        accessor: 'transactionDate', // accessor is the "key" in the data
      },
      {
        Header: 'Budget',
        accessor: 'budget',
      },
      {
        Header: 'Amount',
        accessor: 'amount'
      }
    ],
    []
  )
  const tableInstance = useTable({ columns, data })
 
 const {
   getTableProps,
   getTableBodyProps,
   headerGroups,
   rows,
   prepareRow,
 } = tableInstance
 
 return (
   // apply the table props
   <table className='table' {...getTableProps()}>
     <thead>
       {// Loop over the header rows
       headerGroups.map(headerGroup => (
         // Apply the header row props
         <tr {...headerGroup.getHeaderGroupProps()}>
           {// Loop over the headers in each row
           headerGroup.headers.map(column => (
             // Apply the header cell props
             <th {...column.getHeaderProps()}>
               {// Render the header
               column.render('Header')}
             </th>
           ))}
         </tr>
       ))}
     </thead>
     {/* Apply the table body props */}
     <tbody {...getTableBodyProps()}>
       {// Loop over the table rows
       rows.map(row => {
         // Prepare the row for display
         prepareRow(row)
         return (
           // Apply the row props
           <tr  {...row.getRowProps()}>
             {// Loop over the rows cells
             row.cells.map(cell => {
               // Apply the cell props
               return (
                 <td style={{fontSize:10}} {...cell.getCellProps()}>
                   {// Render the cell contents
                   cell.render('Cell')}
                 </td>
               )
             })}
           </tr>
         )
       })}
     </tbody>
   </table>
 )

  
  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <Table className={classes.table} size="small">
          <TableHead>
            <TableRow>
              <TableCell>Account Name</TableCell>
              <TableCell align="left">Type</TableCell>
              <TableCell align="right">Transaction Date&nbsp;</TableCell>
              <TableCell align="right">Budget %&nbsp;</TableCell>
              <TableCell align="right">Amount&nbsp;</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map(row => (
              <TableRow key={row.name}>
                <TableCell component="th" scope="row">
                  {row.name}
                </TableCell>
                <TableCell align="left">{row.calories}</TableCell>
                <TableCell align="right">{row.fat}</TableCell>
                <TableCell align="right">{row.carbs}</TableCell>
                <TableCell align="right">{row.protein}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    </div>
  );
  
}
