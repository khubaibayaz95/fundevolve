
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Col, Container, Row } from 'react-bootstrap';
import { PieChart } from 'react-minimal-pie-chart';
import BudgtTable from './BudgetTable';

const useStyles = makeStyles({
  card: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

export default function IntentionalTab() {
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;

  const sortyByOptions = [
      'Month',
      'Year to Date',
      '3 Months',
      '6 Months',
      '12 Months',
      'Custom'
  ];

  return (
    <Card className={classes.card}>
      <CardContent style={{
          alignItems:'center'
      }}>
       
        <Typography variant="h6" component="h6" style={{
            float:'left',
            textAlign:'left',
            fontSize:'15px'
        }}>
        <b> The Basics Expenditures</b>
        <br/>
 
        <div>

            <span style={{
                color:'lightgrey',
                textAlign:'left',
                fontSize:'10px'



            }}>
            <label>Page last updated: 07:37 PM ET, Jul 15, 2021</label>


            </span>
        </div>
       

       
        </Typography>
        <p style={{
            float:'right',
            textAlign:'right'
        }}>
            <span style={{
                fontSize:'10px'
            }}><b>Total Transactions</b></span>
            <br/>
            <b>$2961.70</b>
        </p>
        <br/>
        <br/>
     <hr/>
     <br/>


     <Container>
         <Row sm={3}>
             <Col>
             <div style={{

                 height:'200px',
                 width:'200px',
                 textAlign:'center',
                 justifyContent:'center',
                 alignItems:'center',
                 margin:'auto'
             }}>
            
             <PieChart
             lineWidth={15}
            
                data={[
                    
                    { title: 'Dues and Subscriptions', value: 27, color: '#d3dde7' },
                    { title: 'ATM/Cash Withdrawls', value: 68, color: '#3498db' },
                    { title: 'Gasoline/Fuel', value: 5, color: '#909090' },
                ]}
                />

               


             </div>
          
           
             </Col>
             <Col>
             <div style={{
                 float:'left',
                 textAlign:'left',
                 margin:'80px'
             }}>
             <ul style={{
                 fontSize:'16px',
                 listStyleType:'none',
                 marginLeft:'-50%'
             }}>
                 <li><b> <canvas id="circle" width="8" height="8" style={{backgroundColor:"#3498db", marginRight:'15px', borderRadius:'50px'}}></canvas>68% ATM/Cash Withdrawls</b></li>
                 <li><b><canvas id="circle" width="8" height="8" style={{backgroundColor:"#d3dde7", marginRight:'15px', borderRadius:'50px'}}></canvas>27% Dues and Subscriptions</b></li>
                 <li><b><canvas id="circle" width="8" height="8" style={{backgroundColor:"#909090", marginRight:'15px', borderRadius:'50px'}}></canvas>5% Gasoline/Fuel</b></li>
             </ul>


             </div>
             
             </Col>
             <Col>
             <div style={{
                 textAlign:'left',
                 float:'left'
             }}>
             {/* <label><b>What are The Basics?</b></label>
             <br/>
             <p style={{

                 textAlign:'left',
                 float:'left'
             }}>These are everyday fixed expenses that provide you the basic foundation of your life.  </p>
             <br/> */}

             <br/>
             <br/>
            
           


             </div>
             <div style={{
                 borderLeft:'2px solid darkgrey',
                 height:'100%',
                 paddingLeft:'20px',
                 marginLeft: '-20px'
             }}>

                 <label style={{
                     marginTop: '30%'
                 }}>Expenses you planned for or needed to make to accomplish something.</label>


             </div>
             
             </Col>

         </Row>
   <br/>
   <br/>

         <Row sm={2}>
             <Col style={{

                 textAlign:'left',
                 float:'left'
             }}>
                <Typography variant="h6" component="h6" style={{
            float:'left',
            textAlign:'left',
            fontSize:'15px'
        }}>
            Recent Transaction
            </Typography>
             
             </Col>
             <Col style={{

            textAlign:'right',
            float:'right'
            }} >
                
                <select className='form-control' style={{width:'50%', float:'right', borderRadius:0, height:35}} >
                  {
                      sortyByOptions.map((val) => (
                        <option style={{backgroundColor:'lightgray'}}> {val}</option>
                      ))
                  }

                </select>
             
             </Col>


         </Row>
         <hr style={{
             borderTop:'3px solid'
         }}/>

         <Row>

             <BudgtTable/>
          
        
         </Row>
         <Row>
        
         </Row>
         
        <Row>
          <Col md={12}>
            <hr></hr>
          </Col>
         

        <Col style={{textAlign:'left', fontWeight:'bold'}}>
            Total Transactions
        </Col>
        <Col style={{textAlign:'right', float:'right', marginRight:5, fontWeight:'bold'}}>
            2916.0$
        </Col>
         </Row>

      
      
    


     </Container>
       
      
      </CardContent>
      <CardActions>
      
      </CardActions>
    </Card>
  );
}
