import React from "react";
import { Modal, Button, Container } from 'react-bootstrap';

const Step2 = (props) => {
  const { data, handleChange, next, back } = props;
  return (
    <Container>
      <form>
        <p>
          <label className="uppercase fs-10 f-w-b" htmlFor="name">Name your goal</label>
          <br/>
          <input
            type="text"
            name="projectName"
            value={data.projectName}
            className='form-control custom-input f-w-b'
            placeholder="Enter Project Name"
            onChange={handleChange}
          />

          <label style={{marginTop: '40px'}} className="uppercase fs-10 f-w-b">Describe your goal</label>
          <textarea
            name="goalDesc"
            className='form-control custom-input f-w-b'
            placeholder="Tell us about your goal..."
            value={data.gaolDesc}
            onChange={handleChange}
            rows={5}
          >

          </textarea>
        </p>

        <div className="modal-footer-buttons">
          <Button className="min-width-100" size="sm" variant="dark" style={{ float: 'left' }} onClick={back}>&#60;&#60; Back</Button>{" "}
          <Button className="min-width-100" size="sm" variant="dark" style={{ float: 'right' }} onClick={next}>Next &#62;&#62;</Button>
        </div>



      </form>
    </Container>
  );
};
export default Step2;