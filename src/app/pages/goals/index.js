import React, { useState, Fragment, useEffect } from "react";

import { Stepper } from 'react-form-stepper';

import { Link, useHistory } from 'react-router-dom';

import Step1 from "./MultiStep1";
import Step2 from "./MultiStep2";
import Step3 from "./MultiStep3";
import Step4 from "./MultiStep4";
import Step5 from "./MultiStep5";
import Step6 from "./MultiStep6";
import Submit from "./MultiStepSubmit";


import { makeStyles } from '@material-ui/core/styles';
// import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
// import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import PriorityHighIcon from '@material-ui/icons/PriorityHighRounded';



import { Container, Row, Col, Button, Modal, Card, Image, DropdownButton, Dropdown } from 'react-bootstrap';

export const MultiStepForm = () => {

  let history = useHistory();

  const [currentStep, setCurrentStep] = useState(1);
  const sampleArray = ["test1", "test2", "test3"];
  
  const [prvBtn, setPrvBtn] = useState('budget');

  const [formData, setFormData] = useState({
    option: "",
    name: "",
    projectName: "",
    goalDesc: "",
    accountPay: "",
    AccountInfo: '',
    aprMonth: "",
    aprYear: "",
    perMonthPay: "",
    collabGoalA: false,
    collabGoalB: false,
    motivationalQuote: "",
    email: "",
    street: "",
    city: "",
    postcode: "",
    comments: "",
  });
  const ITEM_HEIGHT = 48;
  const options = [
    'Edit',
    'Share',
    'Save',
    'Delete'
  ];


  const [show, setShow] = useState(false);
  const [modalTitle, setModalTitle] = useState("CREATE A GOAL");

  const [goalsCount, setGoalsCount] = useState(1)

  const [ddValue, setDDValue] = useState('Show All')

  const handleChange = (event) => {

    if (event.target && event.target.name) {
      setFormData({
        ...formData,
        [event.target.name]: event.target.value,
      });
    }
    else {
      if (event && event.currentTarget && event.currentTarget.name && event.currentTarget.name.includes('step1button')) {

        let element = event.currentTarget.id;

        document.getElementById(prvBtn).style.background = '#E0E0E0';
        document.getElementById(prvBtn).style.color = '#000'
        setPrvBtn(element)

        document.getElementById(element).style.color = '#fff'
        document.getElementById(element).style.background = 'black';



        setFormData({
          ...formData,
          option: event.currentTarget.id,
        });
      }
      else {
        setFormData({
          ...formData,
          accountPay: event,
        });

      }
    }
  };

  
  const next = () => {
    if (currentStep + 1 == 7) {
      setShow(false)
      setCurrentStep(1);
    }
    else {
      setCurrentStep(currentStep + 1);
    }
  };


  const back = () => {
    setCurrentStep(currentStep - 1);
  };

  const saveForm = () => {
    setShow(false)
    setCurrentStep(1);

    window.location.reload();
  }

  const updateDDVal = (e) => {
    setDDValue(e.target.value)
  }


  const classes = makeStyles({
    card: {
      maxWidth: 345,
    },
    media: {
      height: 140,
    },
  });



  const IsolatedMenu = props => {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);

    return (
      <React.Fragment>
        <IconButton
          aria-label="more"
          aria-controls="long-menu"
          aria-haspopup="true"
          onClick={event => setAnchorEl(event.currentTarget)}
          style={{ float: 'right' }}
        >

          <MoreVertIcon />
        </IconButton>
        <Menu
          id="long-menu"
          anchorEl={anchorEl}
          keepMounted
          open={open}
          onClose={() => setAnchorEl(null)}
        >

          {options.map(id => {
            return (
              <MenuItem onClick={() => {
                setAnchorEl(null);
                if (id === 'Edit') {
                  return (
                    history.push('/goals/1')
                  )
                }
              }}>{id}</MenuItem>
            );
          })}
        </Menu>

      </React.Fragment>
    )
  }


  return (

    <Fragment>
      <Container>
        <div className="upper-grid">
          <div style={{ paddingTop: '1%' }} className="f-w-b" > Sort By</div>
          <DropdownButton className="custom-dd ml-10 mr-10" id="dropdown-item-button" title={ddValue}>
            <Dropdown.Item onClick={(e) => updateDDVal(e)} value="Show All" as="button">Show All</Dropdown.Item>
            <Dropdown.Item onClick={(e) => updateDDVal(e)} value="Goal Type" as="button">Goal Type</Dropdown.Item>
            <Dropdown.Item onClick={(e) => updateDDVal(e)} value="Deadliness" as="button">Deadliness (Closest to Farthest)</Dropdown.Item>
            <Dropdown.Item onClick={(e) => updateDDVal(e)} value="Progress" as="button">Progress (Most to Least)</Dropdown.Item>
            <Dropdown.Item onClick={(e) => updateDDVal(e)} value="Individual Goals" as="button">Individual Goals</Dropdown.Item>
            <Dropdown.Item onClick={(e) => updateDDVal(e)} value="Group Goals" as="button">Group Goals</Dropdown.Item>
          </DropdownButton>

          <Button className="goals-create min-width-100" size="sm" variant="dark" onClick={() => setShow(true)}>
            <div class="circle">+</div> Create Goal
          </Button>

        </div>




        <Card className="goals-card">
          <Card.Body>
            {(goalsCount == 0) && (<Row className="justify-content-md-center">

              <Row style={{ width: '100%' }}>
                <Col className="h-100 text-center mt-5 pt-5">
                  <Image style={{ width: '171px', height: '180px' }} src="media/users/300_21.jpg" roundedCircle />
                </Col>
              </Row>


              <Row style={{ width: '100%' }}>
                <Col className="h-100 text-center mt-5 pt-5">
                  <h6 className="f-w-500">Financial goals are an important step <br />toward becoming financially secure.</h6>
                </Col>
              </Row>
              <Row style={{ width: '100%' }}>
                <Col className="h-100 text-center mt-5 pt-5">
                  <Button className="b-r-20" variant="dark" onClick={() => setShow(true)}>
                    Create Your First Goal
                  </Button>
                </Col>
              </Row>
            </Row>)}

            {(goalsCount != 0) && (<Row className="justify-content-md-center">
              <Card
                // className={classes.card}
                className="goals-inner-card-width"
              >
                <CardActionArea>
                  <div>
                    {
                      <IsolatedMenu id={0} />
                    }
                  </div>
                  <CardMedia
                    className={classes.media}
                    image="/static/images/cards/contemplative-reptile.jpg"
                    title="Contemplative Reptile"
                  />
                  <CardContent>
                    <Typography style={{ fontSize: '10px', fontWeight: 'bold' }} gutterBottom variant="h5" component="p">
                      Your Progress
                    </Typography>
                    <Typography variant="body2" style={{ fontSize: '16px', fontWeight: 'bold' }} color="textSecondary" component="h1">
                      10%
                    </Typography>
                    <Typography variant="body2" style={{ fontSize: '16px', textAlign: 'center', fontWeight: 'bold' }} color="textSecondary" component="h1">
                      <Image style={{ width: '150px', height: '150px' }} src="/media/users/300_21.jpg" />
                    </Typography>
                    <Typography variant="body2" style={{ fontSize: '10px', fontWeight: 'bold', marginBottom: '5px' }} color="textSecondary" component="h1">
                      <p style={{ marginBottom: '5px' }} className="uppercase">Crush credit card debt</p>
                    </Typography>
                    <Typography variant="body2" style={{ fontSize: '12px', fontWeight: 'bold' }} color="textSecondary" component="h1">
                      <p>Pay Off Visa</p>
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>

              <Card
                // className={classes.card}
                className="goals-inner-card-width"
              >
                <CardActionArea>
                  <div>
                    {
                      <IsolatedMenu id={0} />
                    }
                  </div>
                  <CardMedia
                    className={classes.media}
                    image="/static/images/cards/contemplative-reptile.jpg"
                    title="Contemplative Reptile"
                  />
                  <CardContent>
                    <Typography style={{ fontSize: '10px', fontWeight: 'bold' }} gutterBottom variant="h5" component="p">
                      Your Progress
                    </Typography>
                    <Typography variant="body2" style={{ fontSize: '16px', fontWeight: 'bold' }} color="textSecondary" component="h1">
                      10%
                    </Typography>
                    <Typography variant="body2" style={{ fontSize: '16px', textAlign: 'center', fontWeight: 'bold' }} color="textSecondary" component="h1">
                      <Image style={{ width: '150px', height: '150px' }} src="/media/users/300_21.jpg" />
                    </Typography>
                    <Typography variant="body2" style={{ fontSize: '10px', fontWeight: 'bold', marginBottom: '5px' }} color="textSecondary" component="h1">
                      <p style={{ marginBottom: '5px' }} className="uppercase">Take a trip</p>
                    </Typography>
                    <Typography variant="body2" style={{ fontSize: '12px', fontWeight: 'bold' }} color="textSecondary" component="h1">
                      <p>Class Reunion in Toronto</p>
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>

              <Card
                // className={classes.card}
                className="goals-inner-card-width"
              >
                <CardActionArea>
                  <div>
                    {
                      <IsolatedMenu id={0} />
                    }
                  </div>
                  <CardMedia
                    className={classes.media}
                    image="/static/images/cards/contemplative-reptile.jpg"
                    title="Contemplative Reptile"
                  />
                  <CardContent>
                    <Typography style={{ fontSize: '10px', fontWeight: 'bold' }} gutterBottom variant="h5" component="p">
                      Your Progress
                    </Typography>
                    <Typography variant="body2" style={{ fontSize: '16px', fontWeight: 'bold' }} color="textSecondary" component="h1">
                      10%
                    </Typography>
                    <Typography variant="body2" style={{ fontSize: '16px', textAlign: 'center', fontWeight: 'bold' }} color="textSecondary" component="h1">
                      <Image style={{ width: '150px', height: '150px' }} src="/media/users/300_21.jpg" />
                    </Typography>
                    <Typography variant="body2" style={{ fontSize: '10px', fontWeight: 'bold', marginBottom: '5px' }} color="textSecondary" component="h1">
                      <p style={{ marginBottom: '5px' }} className="uppercase">Build a budget</p>
                    </Typography>
                    <Typography variant="body2" style={{ fontSize: '12px', fontWeight: 'bold' }} color="textSecondary" component="h1">
                      <p>Eat out less or dine cheaper</p>
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>

            </Row>)}
          </Card.Body>
        </Card>


      </Container>

      <Modal
        show={show}
        onHide={() => setShow(false)}
        // dialogClassName="modal-90w modal-height"
        aria-labelledby="contained-modal-title-vcenter"
        backdrop="static"
        size="lg"
        centered
      >
        <Modal.Header className="modal-header-bg" closeButton>
          <div style={{ display: 'flex', flexWrap: 'wrap', width: '100%' }}>
            <div style={{ width: '100%', textAlign: 'center' }}><p className="f-w-b">{currentStep >= 2 ? "CRUSH CREDIT CARD DEBT" : "CREATE A GOAL"}</p></div>
            <div style={{ width: '100%' }}>
              <Stepper
                steps={[{ label: currentStep == 1 ? 'Define' : '' }, { label: currentStep == 2 ? 'Describe' : '' }, { label: currentStep == 3 ? 'Specify' : '' }, { label: currentStep == 4 ? 'Collaborate' : '' }, { label: currentStep == 5 ? 'Visualize' : '' }, { label: currentStep == 6 ? 'Get Started' : '' }]}
                activeStep={currentStep - 1}
              />
            </div>
          </div>
        </Modal.Header>

        <Modal.Body className="modal-body-height">
          <Container>
            {currentStep == 1 && (<Step1
              data={formData}
              handleChange={handleChange}
              next={next}
            />)}

            {currentStep == 2 && (<Step2
              data={formData}
              handleChange={handleChange}
              next={next}
              back={back}
            />)}

            {currentStep == 3 && (<Step3
              data={formData}
              handleChange={handleChange}
              next={next}
              back={back}
            />)}

            {currentStep == 4 && (<Step4
              data={formData}
              handleChange={handleChange}
              next={next}
              back={back}
            />)}

            {currentStep == 5 && (<Step5
              data={formData}
              handleChange={handleChange}
              next={next}
              back={back}
            />)}

            {currentStep == 6 && (<Step6
              data={formData}
              handleChange={handleChange}
              next={next}
              back={back}
              saveForm={saveForm}
            />)}
          </Container>
        </Modal.Body>
      </Modal>
    </Fragment>
  )
}

