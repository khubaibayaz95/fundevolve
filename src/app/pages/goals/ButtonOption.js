import React, { useState } from "react";
import { Modal, Button, Container } from 'react-bootstrap';

var prvBtn = 'button1';



const ButtonOption = (props) => {


    const { data, handleChange, next } = props;

    const handleClick = (e) => {

        
    }



    return (
        <div>
            <Button style={{
                borderRadius: '5px',
                backgroundColor: '#E0E0E0',
                height: '80px',
                width: '115px',
                borderColor: '#E0E0E0',
                color: 'black',
                float: 'left',
                margin: '5px 15px 5px 0px',
                textAlign: 'center',
                justifyContent: 'center',
                alignContent: 'center',
                fontSize: '12px'

            }}
                id={props.id}
                name={props.name}
                onClick={handleChange}
            >
                <b>
                    {props.text}
                </b>


            </Button>
        </div>

    );
};
export default ButtonOption;