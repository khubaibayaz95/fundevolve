import React, { useState, Fragment, useEffect } from "react";
import { Link, useHistory, useParams } from 'react-router-dom'
import { Container, Row, Col, Button, Modal, Card, Image, DropdownButton, Dropdown, ProgressBar } from 'react-bootstrap';

import { makeStyles } from '@material-ui/core/styles';
import CardActionArea from '@material-ui/core/CardActionArea';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';


export const EditGoals = () => {
    const { goalid } = useParams()
    const [ddValue, setDDValue] = useState('Show All')
    const [show, setShow] = useState(false);
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    const ITEM_HEIGHT = 48;
    const options = [
        'Edit',
        'Share',
        'Save',
        'Delete'
    ];

    const classes = makeStyles({
        card: {
            maxWidth: 345,
        },
        media: {
            height: 140,
        },
    });

    useEffect(() => {

        if (goalid) {

        }
    }, [goalid])


    function handleClick(event) {
        setAnchorEl(event.currentTarget);
    }

    function handleClose() {
        setAnchorEl(null);
    }

    const updateDDVal = (e) => {
        setDDValue(e.target.value)
    }

    return (

        <Fragment>
            <Container>
                <div className="upper-grid">
                    <div style={{ paddingTop: '1%' }} className="f-w-b" > Sort By</div>
                    <DropdownButton className="custom-dd ml-10 mr-10" id="dropdown-item-button" title={ddValue}>
                        <Dropdown.Item onClick={(e) => updateDDVal(e)} value="Show All" as="button">Show All</Dropdown.Item>
                        <Dropdown.Item onClick={(e) => updateDDVal(e)} value="Goal Type" as="button">Goal Type</Dropdown.Item>
                        <Dropdown.Item onClick={(e) => updateDDVal(e)} value="Deadliness" as="button">Deadliness (Closest to Farthest)</Dropdown.Item>
                        <Dropdown.Item onClick={(e) => updateDDVal(e)} value="Progress" as="button">Progress (Most to Least)</Dropdown.Item>
                        <Dropdown.Item onClick={(e) => updateDDVal(e)} value="Individual Goals" as="button">Individual Goals</Dropdown.Item>
                        <Dropdown.Item onClick={(e) => updateDDVal(e)} value="Group Goals" as="button">Group Goals</Dropdown.Item>
                    </DropdownButton>

                    <Button className="goals-create min-width-100" size="sm" variant="dark" onClick={() => setShow(true)}>
                        <div class="circle">+</div> Create Goal
                    </Button>

                </div>


                <Card className="edit-goals-card">
                    <Card.Body>
                        <Card
                            // className={classes.card}
                            className="edit-goals-inner-card-width"
                        >
                            <CardActionArea>
                                <IconButton
                                    aria-label="More"
                                    aria-controls="long-menu"
                                    aria-haspopup="true"
                                    onClick={handleClick}
                                    style={{ float: 'right' }}
                                >
                                    <MoreVertIcon />
                                </IconButton>
                                <Menu
                                    id="long-menu"
                                    anchorEl={anchorEl}
                                    keepMounted
                                    open={open}
                                    onClose={handleClose}
                                    PaperProps={{
                                        style: {
                                            maxHeight: ITEM_HEIGHT * 4.5,
                                            width: 200,
                                        },
                                    }}
                                >
                                    {options.map(option => (
                                        //onClick={handleClickOptions(option)}
                                        <MenuItem key={option}>
                                            {option}
                                        </MenuItem>
                                    ))}
                                </Menu>
                                <CardMedia
                                    className={classes.media}
                                    image="/static/images/cards/contemplative-reptile.jpg"
                                    title="Contemplative Reptile"
                                />

                                <CardContent>
                                    <Typography className="uppercase m-b-20" style={{ fontSize: '10px', fontWeight: 'bold' }} gutterBottom variant="h5" component="p">
                                        Crush credit card debt
                                    </Typography>
                                    <Typography variant="body2" style={{ fontSize: '16px', textAlign: 'center', fontWeight: 'bold' }} color="textSecondary" component="h1">

                                        <Row>
                                            <Col className="t-a-l">
                                                <label style={{ fontSize: '20px' }}>Pay Off Visa</label>
                                            </Col>
                                            <Col className="text-center">
                                                <Image style={{ width: '80px', height: '80px' }} src="/media/users/300_21.jpg" />
                                            </Col>
                                            <Col className="t-a-l">
                                                <label className="uppercase fs-10">Your Progress</label><br />
                                                <label className="f-w-b">10% on target</label>


                                                <ProgressBar className="pg-bar" striped now={10} key={1} />
                                                <label className="fs-10">$30,600 of $34,000 Needed to Achieve Goal</label>
                                            </Col>
                                        </Row>


                                    </Typography>
                                    <Typography variant="body2" style={{ fontSize: '10px', fontWeight: 'bold', marginBottom: '5px' }} color="textSecondary" component="h1">
                                        <p style={{ marginBottom: '5px' }} className="uppercase">Crush credit card debt</p>
                                    </Typography>
                                    <Typography variant="body2" style={{ fontSize: '12px', fontWeight: 'bold' }} color="textSecondary" component="h1">
                                        <p>Pay Off Visa</p>
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Card>

                    </Card.Body>

                    <Container>
                        <Container>
                            <Row>
                                <Col md="auto" className="t-a-l">
                                    <label className="uppercase fs-10 f-w-b">Goal Amount</label><br />
                                    <label className="font-color-dark f-w-b" style={{ fontSize: '20px' }}><h4>$34,000</h4></label>

                                    <hr className="t-a-l hr-width" />
                                    <label className="uppercase fs-10 f-w-b">Goal Balance</label><br />
                                    <label className="font-color-dark f-w-b" style={{ fontSize: '20px' }}><h4>$30,600</h4></label>

                                    <hr className="t-a-l hr-width" />
                                    <label className="uppercase fs-10 f-w-b">Goal Deadline</label><br />
                                    <label className="font-color-dark f-w-b" style={{ fontSize: '20px' }}><h4>12/34/24</h4></label>
                                    <hr className="t-a-l hr-width" />
                                    <label className="uppercase fs-10 f-w-b">Time Remainin</label><br />
                                    <label className="font-color-dark f-w-b" style={{ fontSize: '20px' }}><h4>1,455 Days</h4></label>

                                </Col>
                                <Col>
                                    <p className="text-desc"><i>"It doesn't matter how slowly you go, as long as you do not stop."</i></p>
                                    <br />
                                    <label className="uppercase fs-10 f-w-b">Goal Definition</label>
                                    <p className="text-desc">I want to pay off my Visa card since it has the highest interest rate.</p>

                                    <br />
                                    <label className="uppercase fs-10 f-w-b">Tasks</label><br />

                                    <label className="checkbox-label">
                                        <input className="custom-checkbox" type="checkbox" name='checkbox' />  It's not a sprint, it's a marathon! Stay commited to your plan.
                                    </label>
                                    <label className="checkbox-label">
                                        <input className="custom-checkbox" type="checkbox" name='checkbox' /> An additional $20 per month towards your credit cards will help you reach your goal two months sooner.
                                    </label>

                                </Col>
                                <Col md="auto" className="t-a-l">
                                    <div class="vl">
                                        <label className="uppercase fs-10 f-w-b">Need Help?</label><br />
                                        <p className="text-desc">Schedule a check-in with your <br />advisor by to review your progress on this goal</p>

                                        <Button className="min-width-100 b-r-20 mb-40" size="sm" variant="dark">Schedule Call</Button> {" "}
                                        <br />
                                        <a href="#">Don't have an advisor?</a>
                                        <br />
                                        <a href="#">Get Matched</a>
                                    </div>
                                </Col>
                            </Row>
                        </Container>

                        <Container>
                            <Row>
                                        <p></p>
                            </Row>
                        </Container>
                    </Container>
                </Card>
            </Container>
        </Fragment>
    )
}