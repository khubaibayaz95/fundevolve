import React, { useState } from "react";
import { Modal, Button, Container, Row, Col, DropdownButton, Dropdown } from 'react-bootstrap';

import PropTypes from 'prop-types';
import deburr from 'lodash/deburr';
import Downshift from 'downshift';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Popper from '@material-ui/core/Popper';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import Chip from '@material-ui/core/Chip';


const Step3 = (props) => {
  const { data, handleChange, next, back } = props;
  const [ddMonthValue, setDDMonthValue] = useState('Month')
  const [ddYearValue, setDDYearValue] = useState('Year')
  const [account, setAccount] = useState('')

  const suggestions = [
    { label: 'Visa' },
    { label: 'Chase Visa' },
    { label: 'Chase Visa 2' }
  ];

  const classes = makeStyles(theme => ({
    root: {
      flexGrow: 1,
      height: 250,
    },
    container: {
      flexGrow: 1,
      position: 'relative',
    },
    paper: {
      position: 'absolute',
      zIndex: 1,
      marginTop: theme.spacing(1),
      left: 0,
      right: 0,
    },
    chip: {
      margin: theme.spacing(0.5, 0.25),
    },
    inputRoot: {
      flexWrap: 'wrap',
    },
    inputInput: {
      width: 'auto',
      flexGrow: 1,
    },
    divider: {
      height: theme.spacing(2),
    },
  }));

  function renderInput(inputProps) {
    const { InputProps, classes, ref, ...other } = inputProps;

    return (
      <TextField
        InputProps={{
          inputRef: ref,
          classes: {
            root: classes.inputRoot,
            input: classes.inputInput,
            background: '<i className="ki ki-bold-more-ver" /> '
          },
          ...InputProps,
        }}
        {...other}
      />
    );
  }

  const onChangeAccount = (value) => {
    setAccount(value);
  }

  const checkValue = (selectedItem) => {

    setAccount(selectedItem);
    handleChange(selectedItem)
  }

  function renderSuggestion(suggestionProps) {
    const { suggestion, index, itemProps, highlightedIndex, selectedItem } = suggestionProps;
    const isHighlighted = highlightedIndex === index;
    const isSelected = (selectedItem || '').indexOf(suggestion.label) > -1;

    if (isSelected) {
      onChangeAccount(suggestion.label)
    }

    return (
      <MenuItem
        {...itemProps}
        key={suggestion.label}
        selected={isHighlighted}
        component="div"
        style={{
          fontWeight: isSelected ? 500 : 400,
        }}
      >
        {suggestion.label}
      </MenuItem>
    );
  }
  renderSuggestion.propTypes = {
    highlightedIndex: PropTypes.number,
    index: PropTypes.number,
    itemProps: PropTypes.object,
    selectedItem: PropTypes.string,
    suggestion: PropTypes.shape({ label: PropTypes.string }).isRequired,
  };

  function getSuggestions(value, { showEmpty = false } = {}) {
    const inputValue = deburr(value.trim()).toLowerCase();
    const inputLength = inputValue.length;
    let count = 0;

    return inputLength === 0 && !showEmpty
      ? []
      : suggestions.filter(suggestion => {
        const keep =
          count < 5 && suggestion.label.slice(0, inputLength).toLowerCase() === inputValue;

        if (keep) {
          count += 1;
        }

        return keep;
      });
  }

  const updateDDMonthVal = (e) => {
    setDDMonthValue(e.target.value)
  }

  const updateDDYearVal = (e) => {
    setDDYearValue(e.target.value)
  }

  return (
    <Container>
      <form>
        <p>
          <label className="uppercase fs-10 f-w-b m-b-20" htmlFor="name">Which account do you want to pay off?</label>
          <br />
          {/* <input
            type="text"
            name="projectName"
            value={data.projectName}
            className='form-control custom-input f-w-b'
            placeholder="Enter Project Name"
            onChange={handleChange}
          /> */}

          {/* <label style={{marginTop: '40px'}} className="uppercase fs-10 f-w-b">Describe your goal</label>
          <textarea
            name="goal-desc"
            className='form-control custom-input f-w-b'
            placeholder="Tell us about your goal..."
            rows={5}
          >

          </textarea> */}



          {account === '' && (<Downshift id="downshift-options">
            {({
              clearSelection,
              getInputProps,
              getItemProps,
              getLabelProps,
              getMenuProps,
              highlightedIndex,
              inputValue,
              isOpen,
              openMenu,
              selectedItem,
            }) => {
              const { onBlur, onChange, onFocus, ...inputProps } = getInputProps({
                onChange: event => {
                  if (event.target.value === '') {
                    clearSelection();
                  }
                },
                onFocus: openMenu,
                placeholder: 'Search account',
              });

              if (selectedItem) {
                checkValue(selectedItem)
              }
              return (
                <div className={classes.container}>
                  {renderInput({
                    fullWidth: true,
                    classes,
                    // InputLabelProps: getLabelProps({ shrink: true }),
                    InputProps: { onBlur, onChange, onFocus },
                    inputProps,
                  })}

                  <div {...getMenuProps()}>
                    {isOpen ? (
                      <Paper className={classes.paper}>
                        {getSuggestions(inputValue, { showEmpty: true }).map((suggestion, index) =>
                          renderSuggestion({
                            suggestion,
                            index,
                            itemProps: getItemProps({ item: suggestion.label }),
                            highlightedIndex,
                            selectedItem,
                          })
                        )}
                      </Paper>
                    ) : null}
                  </div>
                </div>
              );
            }}
          </Downshift>)}
        </p>


        {account !== '' && (<div>
          <Row>
            <Col>
              <label className="uppercase f-w-b fs-10">Account info</label><br />
              <label className="f-w-b">Chase Visa</label>
            </Col>
            <Col md="auto" className="text-center">
              <label className="font-color">Interest Rate</label><br />
              <label className="f-w-b">14.87%</label>
            </Col>
            <Col md="auto" className="text-center">
              <label className="font-color">Min. Payment</label><br />
              <label className="f-w-b">$110.00</label>
            </Col>
            <Col xs lg="2" className="text-center">
              <label className="font-color">Balance</label><br />
              <label className="f-w-b">$34,000.00</label>
            </Col>
          </Row>

          <Row>
            <Col>
              <label style={{ display: 'inline-flex', verticalAlign: 'middle' }} check>
                <input className="custom-checkbox" type="checkbox" name='checkbox' onChange={handleChange} value={data.AccountInfo} /> Yes, it will change to <input className="custom-input-step3 f-w-b" placeholder="%" type="text" /> {" APR in "}
                
                <select onChange={handleChange} name="aprMonth" className="custom-dd month" id="dropdown-item-button">
                  <option value="Month" disabled>Month </option>
                  <option value="January">January</option>
                  <option value="February">February</option>
                  <option value="March">March</option>
                  <option value="April">April</option>
                  <option value="May">May</option>
                  <option value="June">June</option>
                  <option value="July">July</option>
                  <option value="August">August</option>
                  <option value="September">September</option>
                  <option value="October">October</option>
                  <option value="November">November</option>
                  <option value="December">December</option>

                </select>
                <span> </span>
                <select onChange={handleChange} name="aprYear" className="custom-dd year" id="dropdown-item-button">
                  <option value="Year" disabled>Year </option>
                  <option value="2021" >2021</option>
                  <option value="2020" >2020</option>
                  <option value="2019" >2019</option>
                </select>

                {/* <DropdownButton className="custom-dd month" id="dropdown-item-button" title={ddMonthValue}>
                  <Dropdown.Item disabled onClick={(e) => updateDDMonthVal(e)} value="Month" as="button">Month</Dropdown.Item>
                  <Dropdown.Item onClick={(e) => updateDDMonthVal(e)} value="January" as="button">January</Dropdown.Item>
                  <Dropdown.Item onClick={(e) => updateDDMonthVal(e)} value="February" as="button">February</Dropdown.Item>
                </DropdownButton> */}
                
                {/* <DropdownButton placeholder="Month" className="custom-dd year" id="dropdown-item-button" title={ddYearValue}>
                  <Dropdown.Item disabled onClick={(e) => updateDDYearVal(e)} value="Year" as="button">Year</Dropdown.Item>
                </DropdownButton> */}
              </label>
            </Col>
          </Row>
          <Row>
            <Col>
              <label check>
                <input className="custom-checkbox" type="checkbox" name='checkbox' /> No
              </label>
            </Col>
          </Row>
          <Row>
            <Col>
              <label check>
                <input className="custom-checkbox" type="checkbox" name='checkbox' /> I don't know
              </label>
            </Col>
          </Row>


          <Row className="mt-10">
            <Col>
              <label className="uppercase f-w-b fs-10">How much can you pay per month? </label><br />
              <input id="perMonthPay" name="perMonthPay" onChange={handleChange} className="pay-pm-step3 f-w-b" placeholder="$" prefix="$" type="number" />
            </Col>
          </Row>


        </div>)}

        <div className="modal-footer-buttons">
          <Button className="min-width-100" size="sm" variant="dark" style={{ float: 'left' }} onClick={back}>&#60;&#60; Back</Button>{" "}
          <Button className="min-width-100" size="sm" variant="dark" style={{ float: 'right' }} onClick={next}>Next &#62;&#62;</Button>
        </div>

      </form>
    </Container>
  );
};
export default Step3;