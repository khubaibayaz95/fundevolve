import React from "react";
import { Modal, Button, Container } from 'react-bootstrap';
const Step6 = (props) => {
  const { data, handleChange, next, back, saveForm } = props;

  return (
    <Container>
      <form style={{

        margin: 'auto',
        textAlign: 'center',
        justifyContent: 'center'

      }}>
        <p style={{

          margin: 'auto'
        }}>
          <h2 htmlFor="comments">Good Luck With Your New Goal!</h2>
          <br />
          <p>Would you like to see notification alerts of this goal?</p>

          <p>(i.e Progress Reports, Tips, Motivation)</p>


          <div className="modal-footer-buttons-final" style={{
            width: '100%',
            marginTop: '30px'
          }}>
            <Button className="min-width-100" size="sm" variant="dark" onClick={saveForm}>Yes</Button> {" "}
            <Button className="min-width-100" size="sm" variant="dark" onClick={saveForm}>No Thanks</Button>
          </div>
        </p>


      </form>
    </Container>
  );
};
export default Step6;