import React from "react";
import { Modal, Button, Container } from 'react-bootstrap';

import ButtonOption from './ButtonOption';

const Step1 = (props) => {
  const { data, handleChange, next } = props;

  return (
    <Container>
      <form>
        <p>
          <label htmlFor="name" style={{fontSize:15}}><b>I want to...</b></label> <br/>
        <span style={{fontSize:'5px'}}><i>Select one option</i></span>
        </p>
        <div style={{
          height:'200px',
          width:'100%',
          textAlign:'center',
          justifyContent:'center',
          margin:'auto'          
          }}
          >

          <ButtonOption handleChange={handleChange} name="step1button1" id="budget" value="budget" text="BUILD A BUDGET"/>
          <ButtonOption handleChange={handleChange} name="step1button2" id="cardDebt" value="cardDebt" text="CRUSH CREDIT CARD DEBT"/>
          <ButtonOption handleChange={handleChange} name="step1button3" id="loans" value="loans" text="PAY OFF MY LOANS"/>
          <ButtonOption handleChange={handleChange} name="step1button4" id="fund" value="fund" text="CREATE A RAINY DAY FUND"/>
          <ButtonOption handleChange={handleChange} name="step1button5" id="renovateHome" value="renovateHome" text="RENOVATE A HOME"/>
          <br/>
          <ButtonOption handleChange={handleChange} name="step1button6" id="buyHome" value="buyHome" text="BUY A HOME"/>
          <ButtonOption handleChange={handleChange} name="step1button7" id="retirement" value="retirement" text="PLAN FOR RETIREMENT"/>
          <ButtonOption handleChange={handleChange} name="step1button8" id="educationalFund" value="educationalFund" text="CREATE AN EDUCATIONAL FUND"/>
          <ButtonOption handleChange={handleChange} name="step1button9" id="trip" value="trip" text="TAKE A TRIP"/>
          <ButtonOption handleChange={handleChange} name="step1button10" id="other" value="other" text="OTHER"/>


          
          </div>

        <div className="modal-footer-buttons">
          <Button className="min-width-100" size="sm" variant="dark" style={{float: 'right'}} onClick={next}>Next &#62;&#62;</Button>
        </div>
      </form>
    </Container>
  );
};
export default Step1;